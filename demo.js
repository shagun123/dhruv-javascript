"use strict";
// typed programming language
// let data : any = 'hello world';
// let data  = [{},{}]
// function test(a:number,b:string) : object{
// return {"number":a,string:b};
// }
// const value = test(12,'');
// console.log(value);
// interface Video{
//     title:string;
//     desc:string;
//     age:number;
// }
// interface
// const videos : Video[]= [{title:"myvideo",desc:"sdsds",age:123},
// {title:"myvideo",desc:"sdfxc",age:123},
// {title:"myvideo",desc:"sdmsdksm",age:341}
// ]
// enums
var Key;
(function (Key) {
    Key[Key["UP_KEY"] = 0] = "UP_KEY";
    Key[Key["DOWN_KEY"] = 1] = "DOWN_KEY";
    Key[Key["LEFT_KEY"] = 2] = "LEFT_KEY";
})(Key || (Key = {}));
const keyPressed = 0;
if (keyPressed == Key.UP_KEY) {
    console.log('called');
}
